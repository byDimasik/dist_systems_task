import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    repositories {
        maven("http://download.java.net/maven/2")
        maven("http://download.osgeo.org/webdav/geotools/")
        maven("http://repo.boundlessgeo.com/main")
        jcenter()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.21")
        classpath("org.jetbrains.kotlin:kotlin-allopen:1.3.21")
        classpath("org.springframework.boot:spring-boot-gradle-plugin:2.1.0.RELEASE")
    }
}

plugins {
    kotlin("jvm") version "1.3.21"
    id("org.jetbrains.kotlin.plugin.jpa") version "1.3.21"
    id("org.jetbrains.kotlin.plugin.spring") version "1.3.21"
    id("org.springframework.boot") version "2.0.1.RELEASE"
    id("io.spring.dependency-management") version "1.0.7.RELEASE"
}

group = "ru.nsu.fit.shishlyannikov"
version = "1.0-SNAPSHOT"

repositories {
    maven("http://download.java.net/maven/2")
    maven("http://download.osgeo.org/webdav/geotools/")
    maven("http://repo.boundlessgeo.com/main")
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    runtimeOnly("org.postgresql:postgresql")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    compile("org.springframework.boot:spring-boot-starter-web")
    testCompile("org.springframework.boot:spring-boot-starter-test")

    compile(group="org.postgresql", name="postgresql", version="42.2.2")
    compile(group="javax.xml.stream", name="stax-api", version="1.0-2")
    compile("com.vladmihalcea:hibernate-types-52:2.4.3")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}