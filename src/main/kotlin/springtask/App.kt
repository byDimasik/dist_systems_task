package springtask

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import java.io.FileInputStream
import java.sql.Timestamp
import javax.xml.namespace.QName
import javax.xml.stream.XMLInputFactory

@SpringBootApplication
class App


fun main(args : Array<String>) {
    SpringApplication.run(App::class.java, *args)
}
