package springtask.db

import com.vladmihalcea.hibernate.type.basic.PostgreSQLHStoreType
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import org.hibernate.annotations.TypeDefs
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.persistence.*

@Entity(name="test_data")
@TypeDefs(
    TypeDef(name = "hstore", defaultForType = PostgreSQLHStoreType::class, typeClass = PostgreSQLHStoreType::class)
)
data class MyNode (
    @Id
    @Column
    var id : Long? = null,

    @Column
    var version: Int = -1,

    @Column
    var timestamp: String = "",

    @Column
    var uid : Long = -1,

    @Column(name = "username")
    var user : String = "",

    @Column
    var changeset : Long = -1,

    @Column
    var lat : Double = -1.0,

    @Column
    var lon : Double = -1.0,

    @Type(type = "hstore")
    @Column(columnDefinition = "hstore")
    var tags : MutableMap<String, String> = mutableMapOf()
)


@Repository
interface NodeRepository : JpaRepository<MyNode, Long> {
    @Query(value = "SELECT *, earth_distance(ll_to_earth(?2,?1), ll_to_earth(lat, lon)) as node_distance FROM test_data WHERE earth_box(ll_to_earth(?2,?1), ?3) @> ll_to_earth(lat, lon) ORDER by node_distance;",
    nativeQuery = true)
    fun findAllByRadius(lon: Double, lat: Double, radius: Double): List<MyNode>
}
