package springtask.parseNodes

import org.springframework.core.convert.support.DefaultConversionService
import springtask.db.MyNode
import springtask.db.NodeRepository
import java.io.FileInputStream
import javax.xml.stream.XMLInputFactory
import javax.xml.stream.events.Attribute
import javax.xml.stream.events.XMLEvent
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.javaField


fun loadNodes(repo : NodeRepository?, path : String = "/home/bydimasik/Downloads/RU-NVS.osm") {
    val converter = DefaultConversionService()
    val factory = XMLInputFactory.newInstance()
    val reader = factory.createXMLEventReader(FileInputStream(path))

    var node: MyNode? = null

    while (reader.hasNext()) {
        val xmlEvent = reader.nextEvent()

        when (xmlEvent.eventType) {
            XMLEvent.START_ELEMENT -> {
                val startElement = xmlEvent.asStartElement()

                when (startElement.name.localPart) {
                    "node" -> {
                        node = MyNode()

                        startElement.attributes.forEach { attr ->
                            val attribute = attr as Attribute

                            val property = node::class.memberProperties.find { it.name == attribute.name.toString() }
                            if (property is KMutableProperty<*>) {
                                val value = converter.convert(attribute.value, property.javaField!!.type)
                                property.setter.call(node, value)
                            }
                        }
                    }

                    "tag" -> {
                        startElement.attributes.forEach {attr ->
                            val attribute = attr as Attribute
                            node?.tags?.put(attribute.name.toString(), attribute.value.toString())
                        }
                    }
                }
            }

            XMLEvent.END_ELEMENT -> {
                val endElement = xmlEvent.asEndElement()

                when (endElement.name.localPart) {
                    "node" -> {
                        node?.let {
                            repo?.save(node)
                        }
                    }
                }
            }
        }
    }
}


fun main(args : Array<String>) {
    loadNodes(null)
}