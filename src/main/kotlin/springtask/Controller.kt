package springtask

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.*
import springtask.db.MyNode
import springtask.db.NodeRepository
import springtask.parseNodes.loadNodes


@RestController
class Controller(private val nodeRepository: NodeRepository) {
    @GetMapping("/initdb")
    fun initDatabase(@RequestParam(value = "path", defaultValue = "/home/bydimasik/Downloads/RU-NVS.osm") path: String) {
        loadNodes(nodeRepository, path = path)
    }

    @PostMapping("/create")
    fun createNode(@RequestBody node: MyNode): MyNode = nodeRepository.save(node)

    @GetMapping("/read")
    fun readNodes(
        @RequestParam(value = "page", defaultValue = "0") page: Int,
        @RequestParam(value = "size", defaultValue = "1000") size: Int
    ): Page<MyNode> = nodeRepository.findAll(PageRequest.of(page, size))

    @PostMapping("/update")
    fun updateNode(@RequestBody node: MyNode): MyNode = createNode(node)

    @DeleteMapping("/delete/{id}")
    fun deleteNode(@PathVariable("id") id: Long) = nodeRepository.deleteById(id)

    @GetMapping("/find")
    fun findInRadius(
        @RequestParam(value = "lon") lon: Double,
        @RequestParam(value = "lat") lat: Double,
        @RequestParam(value = "radius") radius: Double
    ): List<MyNode> {
        return nodeRepository.findAllByRadius(lon, lat, radius)
    }
}